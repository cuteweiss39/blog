<?php include("path.php"); ?>
<?php include(ROOT_PATH . '/app/controllers/users.php'); 
guestsOnly();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <!-- Custom CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
  <title>登入</title>
</head>
<body>
  <!-- header -->
  <?php include(ROOT_PATH . "/app/includes/header.php"); ?>
  <!-- // header -->


  <div class="auth-content">
    <form action="login.php" method="post">
      <h3 class="form-title">登入</h3>
      <!-- <div class="msg error">
        <li>Username required</li>
      </div> -->
      <?php include(ROOT_PATH . '/app/helpers/formErrors.php') ?>

      <div>
        <label>使用者名稱</label>
        <input type="text" name="username" value="<?php echo $username; ?>" class="text-input">
      </div>
      <div>
        <label>密碼</label>
        <input type="password" name="password" value="<?php echo $password; ?>" class="text-input">
      </div>
      <div>
        <button type="submit" name="login-btn" class="btn">登入</button>
      </div>
      <p class="auth-nav">or <a href="register.php">註冊</a></p>
    </form>
  </div>
  <!-- JQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="assets/js/scripts.js"></script>
</body>
</html>