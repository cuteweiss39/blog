<?php 
function usersOnly($redirect = 'index.php'){
if(empty($_SESSION['id']))
{
$_SESSION['message']= '請先登入唷！';
$_SESSION['type'] = 'error';
header('location: ' . BASE_URL . $redirect);
exit(0);
}

}

function adminOnly($redirect = '/index.php'){
    if(empty($_SESSION['id']) || empty($_SESSION['admin']))
    {
    $_SESSION['message']= '沒有權限唷！';
    $_SESSION['type'] = 'error';
    header('location: ' . BASE_URL . $redirect);
    exit(0);
    }

}

function guestsOnly($redirect = '/index.php'){
    if(isset($_SESSION['id']))
    {
    header('location: ' . BASE_URL . $redirect);
    exit(0);
    }

}

?>