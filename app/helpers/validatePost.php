<?php

function validatePost($post)
{
    
    $errors = array();
    if(empty($post['title'])){
    array_push($errors,'請輸入標題');
    }

    if(empty($post['body']) || $post['body']=='<p>&nbsp;</p>'){
        array_push($errors,'請輸入內容');
 }

  if(empty($post['topic_id'])){
         array_push($errors,'請選擇主題');
  }

$existingPost=selectOne('posts', ['title'=>$post['title']]);
if(isset($existingPost)){

    if(isset($post['update-post']) && $existingPost['id']!= $post['id']){
     array_push($errors,'標題已使用，請重新輸入');
 }
 
 if(isset($post['add-post'])){

    array_push($errors,'標題已使用，請重新輸入');
 
 }

}

    return $errors;
}
