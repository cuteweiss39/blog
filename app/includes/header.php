 <!-- header -->
 <header class="clearfix">
    <div class="logo">
      <a href="<?php echo BASE_URL . '/index.php' ?>">
        <h1 class="logo-text"><span>Naco</span>Blog</h1>
      </a>
    </div>
    <div class="fa fa-reorder menu-toggle"></div>
    <nav>
      <ul>
        <li><a href="<?php echo BASE_URL . '/index.php' ?>">Home</a></li>


        <?php if (isset($_SESSION['id'])): ?>
        <li>
          <a href="#" class="userinfo">
            <i class="fa fa-user"></i>
            <?php echo $_SESSION['username']; ?>
            <i class="fa fa-chevron-down"></i>
          </a>
          <ul class="dropdown">
          <?php if($_SESSION['admin']): //儀錶板限制顯示 ?> 
            <li><a href="<?php echo BASE_URL . '/admin/dashboard.php' ?>">儀錶板</a></li>
          <?php endif; ?>
            <li><a href="<?php echo  BASE_URL . '/logout.php'  ?>" class="logout">登出</a></li>
          </ul>
        </li>
        <?php else: ?>
        <li><a href="<?php echo BASE_URL . '/register.php' ?>">註冊</a></li>
        <li><a href="<?php echo BASE_URL . '/login.php' ?>">登入</a></li>

        <?php endif; ?>
      </ul>
    </nav>
  </header>
  <!-- // header -->