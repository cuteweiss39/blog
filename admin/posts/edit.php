<?php include("../../path.php"); ?>
<?php include(ROOT_PATH . '/app/controllers/posts.php');
adminOnly();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <!-- Custom Styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <!-- Admin Styling -->
  <link rel="stylesheet" href="../../assets/css/admin.css">
  <title>後台 - 編輯文章</title>
</head>
<body>
  <!-- header -->
  <?php include(ROOT_PATH . "/app/includes/adminHeader.php"); ?>
  <!-- // header -->
  <div class="admin-wrapper clearfix">
    <!-- Left Sidebar -->
    <?php include(ROOT_PATH . "/app/includes/adminSideber.php"); ?>

    <!-- // Left Sidebar -->
    <!-- Admin Content -->
    <div class="admin-content clearfix">
      <div class="button-group">
        <a href="create.php" class="btn btn-sm">新增文章</a>
        <a href="index.php" class="btn btn-sm">文章管理</a>
      </div>
      <div class="">
        <h2 style="text-align: center;">編輯文章</h2>

        <?php include(ROOT_PATH . '/app/helpers/formErrors.php') ?>

        <form action="edit.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $id ?>">

        <div class="input-group">
            <label>標題</label>
            <input type="text" name="title" value="<?php echo $title ?>" class="text-input">
          </div>
          
          <div>
            <label>內容</label>
            <textarea name="body" id="body"><?php echo $body ?></textarea>
          </div>
        
          <div>
          <label>圖片</label>
          <input type="file" name="image" class="text-input"></input>
          </div>

          <div class="input-group">
            <label>主題</label>
            <select class="text-input" name="topic_id">
              <option></option>
              <?php foreach($topics as $key=>$topic): ?>
              <?php if(!empty($topic_id) && $topic_id==$topic['id']):  ?>
                <option selected value="<?php echo $topic['id'] ?>"><?php echo $topic['name'] ?></option>
              <?php else: ?>
                <option  value="<?php echo $topic['id'] ?>"><?php echo $topic['name'] ?></option>
              <?php endif; ?>

              <?php endforeach; ?>

            </select>
          </div>

          <div>
          <?php if(empty($published) && $published == 0): ?>
          <label>
          <input type="checkbox" name="published">
          發布
          </label>
          <?php else: ?>
            <label>
          <input type="checkbox" name="published" checked>
          發布
          </label>
          <?php endif; ?>
          </div>
          
          <div class="input-group">
            <button type="submit" name="update-post" class="btn">保存</button>
          </div>
        </form>
      </div>
    </div>
    <!-- // Admin Content -->
  </div>
  <!-- JQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- CKEditor 5 -->
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <!-- Custome Scripts -->
  <script src="../../assets/js/scripts.js"></script>
</body>
</html>