<?php include("../../path.php"); ?>
<?php include(ROOT_PATH . '/app/controllers/users.php');
adminOnly();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <!-- Custom Styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <!-- Admin Styling -->
  <link rel="stylesheet" href="../../assets/css/admin.css">
  <title>後台 - 使用者管理</title>
</head>
<body>
  <!-- header -->
  <?php include(ROOT_PATH . "/app/includes/adminHeader.php"); ?>
  <!-- // header -->
  <div class="admin-wrapper clearfix">
    <!-- Left Sidebar -->
    <?php include(ROOT_PATH . "/app/includes/adminSideber.php"); ?>
    <!-- // Left Sidebar -->
    <!-- Admin Content -->
    
    <div class="admin-content clearfix">
      <div class="button-group">
        <a href="create.php" class="btn btn-sm">新增使用者</a>
        <a href="index.php" class="btn btn-sm">使用者管理</a>
      </div>
      <div class="">
        <h2 style="text-align: center;">使用者管理</h2>

        <?php include(ROOT_PATH . '/app/includes/message.php'); ?>

        <table>
          <thead>
            <th>No.</th>
            <th>使用者</th>
            <th colspan="3">Action</th>
          </thead>
          <tbody>
           <?php foreach($admin_users as $key=> $user): ?>
            <tr>
            <td><?php echo $key+1; ?></td>
            <td><?php echo $user['username']; ?></td>
            <td><?php echo $user['email']; ?></td>
            <td><a href="edit.php?id=<?php echo $user['id']; ?>" class="edit">編輯</a></td>
            <td><a href="index.php?delete_id=<?php echo $user['id']; ?>" class="delete">刪除</a></td>
            </tr>
            <?php endforeach;  ?>
          </tbody>
        </table>
      </div>
    </div>


    <!-- // Admin Content -->
  </div>
  <!-- JQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="../../assets/js/scripts.js"></script>
</body>
</html>